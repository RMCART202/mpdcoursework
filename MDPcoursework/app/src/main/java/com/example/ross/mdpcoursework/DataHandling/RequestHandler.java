package com.example.ross.mdpcoursework.DataHandling;


import android.util.Log;

import java.util.ArrayList;

/*
    Created on the 2/3/2018
     Created by Ross McArthur
 *   Matriculation Number: S1429389

    CLASS: Acts as an interface for aquiring specific subsets of data
 */
public class RequestHandler {

    //Gets the accident feed as a list of items
    public ArrayList<FeedData> getAccidents(){
        RSSPuller source = new RSSPuller();
        String conn = source.getAccidentsURL();

        ArrayList<FeedData> accidents =  getFeed(conn, false);

        return accidents;
    }

    //Gets the planned roadworks feed as a list of items
    public ArrayList<FeedData> getPlanned(){
        RSSPuller source = new RSSPuller();
        String conn = source.getUrl_planned_roadworks();


        return getFeed(conn, true);
    }

    //Gets the current roadworks feed as a list of items
    public ArrayList<FeedData> getRoadworks(){
        RSSPuller source = new RSSPuller();
        String conn = source.getUrl_roadworks();


        return getFeed(conn,true);
    }


    //Gets feed data of any type, generic pull function
    public ArrayList<FeedData> getFeed(String conn, boolean dates){

        XMLParser parser = new XMLParser();

        ArrayList<FeedData> feedData= parser.run(conn, dates);


        if(feedData.size()!= 0) return feedData;
        else {

            Log.e("Error", "No Accident data found");

            return feedData;
        }

    }
}
