package com.example.ross.mdpcoursework.DataHandling;


import com.example.ross.mdpcoursework.ListEntries;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Ross McArthur
 * Matriculation Number: S1429389
 */

//POJO: Stores all contents of a single item of  feed data
public class FeedData {

    private int ID;

    private String Title;

    private String Description;

    private String Location;

    private int Duration;

    private String PublicationDate;

    private Date startDate;

    private Date endDate;

    private double Latituide;

    private double Longitude;

    public FeedData() {

    }


    // Getters and setters

    public String getPublicationDate() {
        return PublicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        PublicationDate = publicationDate;
    }

    public String getDescription() {
        return Description;
    }

    public String getTitle() {
        return Title;
    }

    public String getLocation() {
        return Location;
    }

    public void setDescription(String description) {
        Description = description;
    }

    //Sets the location properties for longitude and latitude co-ordinates
    public void setLocation(String location) {

        //set location properties
        Location = location;

        //get the starting co-ordinates up until the space between the two values
        String lat_string = location.substring(0, location.indexOf(" "));
        //get the longitude co-ordinates from the gap in values onwards
        String lon_string = location.substring(location.indexOf(" "));

        //Convert values to double
        setLatituide(Double.parseDouble(lat_string));
        setLongitude(Double.parseDouble(lon_string));
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getDuration() {
        return Duration;
    }

    public double getLatituide() {
        return Latituide;
    }

    public void setLatituide(double latituide) {
        Latituide = latituide;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }


    //Sets the duration difference between the start date and end date
    public void setDuration(){
        //Calculate the difference in milliseconds
        long diff =  this.endDate.getTime()- this.startDate.getTime();

        //Convert Millisecond to day difference
        long duration = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

        this.Duration = (int) duration;
    }


    //Converts the string of dates to date format
    public void setDates(){

        String description = this.getDescription();

        //Attempt to set start and end dates, otherwise print error to log
        try {
            setStartDate(description);
            setEndDate(description);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    //Overwritten constructor sets parameters
    public FeedData(String title, String description, String location, String publicationDate)
    {
        Title = title;
        Description = description;
        this.Location = location;
        PublicationDate= publicationDate;
    }



    public Date getStartDate() {
        return startDate;
    }

    //Set the start date of a feed data entry
    public void setStartDate(String date) throws ParseException {

        //Format Date
        SimpleDateFormat format = new SimpleDateFormat(" EEEE, dd MMMM yyyy ");

        //Find start date based on the index of the start date tag and the
        // position before the end date tag
        String start = date.substring(date.indexOf("Start Date:"), date.indexOf("End Date:"));

        start = start.replace("Start Date:", "");

        start = start.substring(0,start.lastIndexOf("-"));
        //Convert the date from string to Date
        Date startDate = format.parse(start);

        this.startDate = startDate;

    }


    public Date getEndDate() {
        return endDate;
    }

    //Sets the end date of feed data entry
    public void setEndDate(String date) throws ParseException {

        //Format date
        SimpleDateFormat format = new SimpleDateFormat(" EEEE, dd MMMM yyyy ");
        //Find the values after the end date tag
        String end = date.substring(date.indexOf("End Date:"));
        //Remove the end date tag
        end = end.replace("End Date:", "");
        //Parse the date
        Date endDate = format.parse(end);
        this.endDate = endDate;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
}
