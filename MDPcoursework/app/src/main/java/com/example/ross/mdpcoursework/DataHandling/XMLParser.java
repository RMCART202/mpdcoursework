package com.example.ross.mdpcoursework.DataHandling;

import android.util.Log;

import com.example.ross.mdpcoursework.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by Ross McArthur
 * Matriculation Number: S1429389
 */

//Class handles the parsing of data from rss feeds
public class XMLParser  {

    //Attributes
    ArrayList<FeedData> items = new ArrayList<>();

    private boolean requireDates = false;
    private boolean done = false;

    //Process runs thread and waits until the process if complete
    public ArrayList<FeedData> run(String feed, boolean requireDates){

        this.requireDates = requireDates;

        //attempt to run task and wait for results
        try
        {

            startTask(feed);
            return getData();

        }
        catch (Exception e){}

        return null;
    }

    //Listener awaits the thread to complete
    private ArrayList<FeedData> getData() {

        while(!done) {
            if (done) {
                return items;
            }
        }
        return  items;
    }

    //Initiates the thread
    private void startTask(String path) throws InterruptedException {

        new Thread(new Task(path)).start();


    }

    //Class handles the parsing of XML data
    public class Task implements Runnable {
        private String path;

        //Constructor
        public Task(String path) {
            this.path = path;

        }

        //Task operator runs the parsing operation
        @Override
        public void run(){

            URL aurl;
            URLConnection yc;
            BufferedReader in = null;
            String inputLine = "";

            //Attempt parsing
            try {

                //Get path and begin the request
                aurl = new URL(path);
                yc = aurl.openConnection();
                in = new BufferedReader(new InputStreamReader(yc.getInputStream()));

                //Initate xml parser
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();

                factory.setNamespaceAware(true);

                XmlPullParser xpp = factory.newPullParser();

                xpp.setInput(in);

                int eventType = xpp.getEventType();

                //Initiate parameters for looping through each entry
                FeedData feedEntry = new FeedData();
                String field= "";
                boolean entity = false;

                //Until the received feed document is completed
                while (eventType != XmlPullParser.END_DOCUMENT)
                {

                    //Where the document has begun, log the event
                    if(eventType == XmlPullParser.START_DOCUMENT)
                    {

                        System.out.println("Start document");

                        Log.e("MyTag","Start document");

                    }
                    else
                        //When the current read position is a start tag
                    if(eventType == XmlPullParser.START_TAG)
                    {
                        //Get the tag name
                        String temp = xpp.getName();

                        //If the tag is an item, begin pulling feed data tags
                        if(temp.equals("item")) {

                            entity = true;
                        }

                        field = temp;
                    }
                    else
                        //If the current position is a closing tag
                    if(eventType == XmlPullParser.END_TAG)
                    {
                        //get the tag name
                        String temp = xpp.getName();
                        //if the end tag is an item type, finalise the entry's details and
                        // add it to the list
                        if(temp.equals("item")){

                            if(requireDates) {
                                feedEntry.setDates();
                                feedEntry.setDuration();
                            }

                            feedEntry.setID(items.size());
                            items.add(feedEntry);

                            entity = false;

                            feedEntry = new FeedData();
                        }

                    }
                    else
                        //Where the current position is a text value
                    if(eventType == XmlPullParser.TEXT)
                    {
                        //set value
                        String temp = "";
                        temp= xpp.getText();


                        //Remove HTML tags
                        temp = temp.replaceAll("br /", " ");

                        //Where the text is not empty
                        if(!temp.contains("\n")) {

                            //the field is going to be used within an entity
                            if (entity) {
                                //check tag and set entity's corresponding attribute to the text value
                                switch (field) {
                                    case "title":
                                        feedEntry.setTitle(temp);
                                        break;
                                    case "description":
                                        String text = feedEntry.getDescription();
                                        if(text==null) feedEntry.setDescription(temp);
                                        else feedEntry.setDescription(text+ temp);
                                        break;
                                    case "point":
                                        feedEntry.setLocation(temp);
                                        break;
                                    case "pubDate":
                                        feedEntry.setPublicationDate(temp);
                                    default:
                                        break;
                                }
                            }

                        }

                        //Mark progress in log
                        System.out.println("Text "+temp);

                        Log.e("MyTag","Text is "+temp);
                    }




                    //Proceed to next position
                    eventType = xpp.nextToken();

                } // End of while



                in.close();

                done = true;

            }
            //Log errors
            catch (IOException ae)
            {
                Log.e("MyTag", "ioexception");
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }


        }



    }
}
