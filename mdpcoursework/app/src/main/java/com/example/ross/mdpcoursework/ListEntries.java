package com.example.ross.mdpcoursework;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.ross.mdpcoursework.DataHandling.FeedData;
import com.example.ross.mdpcoursework.DataHandling.RequestHandler;



import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by Ross McArthur
 * Matriculation Number: S1429389
 *
 * Class handles the listing of any feed data
 * */

public class ListEntries extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {

    //Attributes
    private TextView txtNoContent;
    private ListView listView;
    private ProgressBar prg;
    private Button btn_back, btn_date, btn_search, btnClear;
    private String path;
    private Boolean timeQuery= false;
    private EditText edt_search;

    private ArrayList<FeedData> items = new ArrayList<FeedData>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_list_entries);



        //Set widget references from view
        edt_search = (EditText)findViewById(R.id.edt_search);

        prg = (ProgressBar)findViewById(R.id.prg_entry);

        txtNoContent = (TextView)findViewById(R.id.txt_noContent);

        btn_back = (Button)findViewById(R.id.btn_return);

        btn_back.setOnClickListener(this);

        btn_date = (Button)findViewById(R.id.btn_searchDate);

        btn_date.setOnClickListener(this);

        btnClear = (Button)findViewById(R.id.btn_SearchClear);
        btnClear.setOnClickListener(this);

        btn_search = (Button)findViewById(R.id.btn_searchEntries);
        btn_search.setOnClickListener(this);

        listView = (ListView) findViewById(R.id.list_view);

        listView.setOnItemClickListener(this);

        //Pull user choice from previous action
        String choice = getIntent().getStringExtra("choice");
        path =choice;

        establishList();

        //when the user has selected either the planned or current roadworks, enable date queries
        if(timeQuery){
            btn_date.setVisibility(View.VISIBLE);
        }



    }

    //When a user selects a list item
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {

        //get item selected's unique ID and send it to the details page as a parameter
        int itemId = items.get(pos).getID();
        Intent path = new Intent(this, DataDetails.class);
        path.putExtra("item", itemId);
        path.putExtra("path", this.path);
        ListEntries.this.startActivity(path);
        ListEntries.this.finish();



    }

    //When a button is pressed, identify the button and redirect to their corresponding task
    @Override
    public void onClick(View view) {
        int state = view.getId();

        //Check the button pressed and handle their task
        switch(state){
            case R.id.btn_searchEntries:
                //gets the value in the edit text and sends it to be queried
                String entry = edt_search.getText().toString();
                search(entry);
                break;
            case R.id.btn_searchDate:
                selectDate();
                break;
            case R.id.btn_return:
                back();
                break;

            case R.id.btn_SearchClear:
                establishList();
                break;
        }

    }

    //Method handles the input of dates
    private void selectDate() {

        //get current date values
        final Calendar c = Calendar.getInstance();
        final int year = c.get(Calendar.YEAR);
        final int month = c.get(Calendar.MONTH);
        final int day = c.get(Calendar.DAY_OF_MONTH);

        //Display a user interface for handling date selection for a user
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int yearCase, int monthOfYearCase, int dayCase) {

                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, yearCase);
                calendar.set(Calendar.DAY_OF_MONTH, dayCase);
                calendar.set(Calendar.MONTH, monthOfYearCase);
                DateSearch(calendar.getTime());

            }
        }, year, month, day);

        datePickerDialog.show();
    }

    //Method handles the date query
    public void DateSearch(Date choice){

        ArrayList<FeedData> searchCase= new ArrayList<FeedData>();

        //Check each entry item to see if the date is contained within the timespan
        for (FeedData item: items) {
            if(choice.before(item.getEndDate())&& choice.after(item.getStartDate())){
                //if that is the case then add to the results ArrayList
                searchCase.add(item);
            }
        }

        //If the search is not empty, display results
        if(searchCase.size()!=0) {
            items = searchCase;
            listView.setVisibility(View.VISIBLE);
            txtNoContent.setVisibility(View.GONE);

        }
        //otherwise display no content message
        else {

            listView.setVisibility(View.GONE);
            txtNoContent.setVisibility(View.VISIBLE);
        }
        //Fill entry with search results
        ArrayAdapter adapter = new FeedArrayAdapter(this, 0, searchCase);

        listView.setAdapter(adapter);
    }

    //Method handles the search title query
    public void search(String query){

        ArrayList<FeedData> searchCase= new ArrayList<FeedData>();


        //For every item, check that the feed item contains part of the search case
        for (FeedData item: items) {
            if(item.getTitle().contains(query)){
                //If that is the case, add item to search results
                searchCase.add(item);
            }
        }

        //If the search case is not empty display list
        if(searchCase.size()!=0) {
            items =searchCase;
            listView.setVisibility(View.VISIBLE);
            txtNoContent.setVisibility(View.GONE);

        }
        //Otherwise display error message
        else {
            listView.setVisibility(View.GONE);
            txtNoContent.setVisibility(View.VISIBLE);
        }

        //Fill list with search cases
        ArrayAdapter adapter = new FeedArrayAdapter(this, 0, searchCase);

        listView.setAdapter(adapter);


    }

    //Method handles the setting of default list values
    public void establishList()
    {

        //Initialise request handler
        RequestHandler rs = new RequestHandler();

        //When the user choice is accident, get accident data
        if(path.equals("accident")){
            items =rs.getAccidents();
            timeQuery = false;
        }
        //if the user choice is planned, get planned roadwork data
        else if (path.equals("planned")){
            items = rs.getPlanned();
            timeQuery=true;
        }
        //if the user choice is roadworks, get current roadwork data
        else if (path.equals("roadworks")){
            items = rs.getRoadworks();
            timeQuery = true;
        }
        else items = rs.getAccidents();


        ArrayAdapter adapter = new FeedArrayAdapter(this, 0, items);

        //Whilst there are items available, display all items of the feed
        if(items.size()!=0) {

            txtNoContent.setVisibility(View.GONE);


            prg.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);


        }

        //Otherwise display no content message
        else {
            txtNoContent.setVisibility(View.VISIBLE);
            txtNoContent.setText("Sorry! Looks like the content is not available at the moment");
        }

        listView.setAdapter(adapter);
    }


    //Method returns user to home page
    public void back(){

        Intent i = new Intent(this, MainActivity.class);


        startActivity(i);
        this.finish();
    }
}
