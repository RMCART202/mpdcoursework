package com.example.ross.mdpcoursework;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.ross.mdpcoursework.DataHandling.FeedData;
import com.example.ross.mdpcoursework.DataHandling.RequestHandler;

import java.net.InetAddress;
import java.util.LinkedList;

/**
 * Created by Ross McArthur
 * Matriculation Number: S1429389
 *
 * Class handles the operations of redirecting the user to different paths on the application
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //Attributes
    private Button btnAccidents, btnPlanned, btnRoadworks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Set references to the xml values in the layout
        btnAccidents = (Button) findViewById(R.id.btn_accidents);
        btnPlanned = (Button)findViewById(R.id.btn_planned);
        btnRoadworks = (Button)findViewById(R.id.btn_unplanned);

        btnRoadworks.setOnClickListener(this);
        btnPlanned.setOnClickListener(this);
        btnAccidents.setOnClickListener(this);



    }

    //Method handles the button operations of the page
    @Override
    public void onClick(View view) {

        //Checks for an internet connection and if failed, displays error message
        if(!checkInternetService()){
            Toast.makeText(getApplicationContext(),
                    "Error: no Internet connection found, please ensure you have WIFI or Mobile Data",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        String path= "";

        //Where the button Id matches the ones below set the path made by the user
        switch(view.getId()){

            case R.id.btn_accidents:
                path = "accidents";
                break;
            case R.id.btn_planned:
                path="planned";
                break;

            case R.id.btn_unplanned:
                path="roadworks";
                break;
        }

        //Redirect to the list page based on the users choice of path
        Intent i = new Intent(this, ListEntries.class);
        i.putExtra("choice", path);

        startActivity(i);
        this.finish();
    }

    //function checks for an internet connection
    public boolean checkInternetService(){
        //attempt to get a connection to the internet
        try {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
        }
        //When the attempt fails, no internet connection was found
        catch(Exception ex){
            return false;
        }

    }
}
