package com.example.ross.mdpcoursework;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ross.mdpcoursework.DataHandling.FeedData;
import com.example.ross.mdpcoursework.DataHandling.RequestHandler;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by Ross McArthur
 * Matriculation Number: S1429389
 */
//Class handles a single selected item for further details
public class DataDetails extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    //Set attributes
    private GoogleMap gMap;
    private Button back;
    private TextView txt_Title, txt_Description, txt_PubDate;
    private FeedData item;
    private SupportMapFragment mapFragment;


    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_details);

        //Set the button references
        back = (Button) findViewById(R.id.btn_detail_back);
        back.setOnClickListener(this);

        //Set the map fragment references
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        //Set textbox references
        txt_Title = (TextView) findViewById(R.id.txt_detail_title);
        txt_Description = (TextView) findViewById(R.id.txt_detail_description);
        txt_PubDate = (TextView) findViewById(R.id.txt_detail_pubDate);


        //Attempt to set context of the map
        try {
            MapsInitializer.initialize(getApplicationContext());

        } catch (Exception e) {

        }

        //Pull path from transfer parameter
        int id = getIntent().getIntExtra("item", 0);
        path = getIntent().getStringExtra("path");

        //Initate search parameters and RSS pull values
        RequestHandler rs = new RequestHandler();
        ArrayList<FeedData> data = new ArrayList<FeedData>();

        if (path.equals("accident")) data = rs.getAccidents();
        else if (path.equals("planned")) data = rs.getPlanned();
        else if (path.equals("roadworks")) data = rs.getRoadworks();
        else data = rs.getAccidents();

        item = new FeedData();

        //For every feed entry, check that the value's ID matches the one passed in the transfer
        for(FeedData entry: data){
            if(id==entry.getID()) item = entry;

        }

        //If an items is found, set values of  textbox
        if (item != null) {

            txt_Title.setText(item.getTitle());
            txt_Description.setText("Description: "+ item.getDescription());
            txt_PubDate.setText("Published on: "+ item.getPublicationDate());

        }
        //Otherwise return to list items
        else {
            Intent i = new Intent(this, ListEntries.class);
            i.putExtra("choice", path);

            startActivity(i);
            this.finish();
        }

        //Set map values
        mapFragment.getMapAsync(this);


    }

    //Handles the redirection to the list page of the application
    @Override
    public void onClick(View view) {
        Intent i = new Intent(this, ListEntries.class);
        i.putExtra("choice", path);

        startActivity(i);
        this.finish();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        //Initialise Map displaying values
        gMap = googleMap;

        //Where permissions are not available, return error
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {




                //Begin request to enable permissions
                AlertDialog.Builder builder = new AlertDialog.Builder(this );

                builder.setTitle("No location permissions found");  // No location message
                builder.setMessage("would you like to add these permissions"); // prompt user
                //when a user presses yes, the application will redirect to the permissions activity
                builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);


                    }

                });

                //otherwise, the application will not persuit using the map feature
                builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mapFragment.getView().setVisibility(View.GONE);

                        Toast.makeText(getApplicationContext(), "Map facilities disabled", Toast.LENGTH_SHORT).show();
                    }
                });



                builder.create().show();





            return;

        }
        //Otherwise set the map marker values to the latitude and longitude of the feed entry
        gMap.setMyLocationEnabled(true);
        LatLng pos = new LatLng(item.getLatituide(), item.getLongitude());
        gMap.addMarker(new MarkerOptions().position(pos).title(item.getTitle())
                .snippet(item.getDescription()));

        //Set the view of the camera, to focus on the marker outlined in the above code
        CameraPosition cameraPosition = new CameraPosition.Builder().target(pos).zoom(12).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        onMapReady(gMap);
    }
}
