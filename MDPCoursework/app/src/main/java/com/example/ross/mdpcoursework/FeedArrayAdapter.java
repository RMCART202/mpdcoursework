package com.example.ross.mdpcoursework;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.ross.mdpcoursework.DataHandling.FeedData;

import java.util.ArrayList;
import java.util.List;

/**
 Created by Ross McArthur
 * Matriculation Number: S1429389
 * Class handles the process of setting the individual items in the list view
 */

public class FeedArrayAdapter extends ArrayAdapter<FeedData>{

    //Attributes
    private Context context;
    private List<FeedData> feed;

    //Constructor handles the initialisation of the list adapter settings
    public FeedArrayAdapter(Context con, int resource, ArrayList<FeedData>assets){
        super(con, resource, assets);
        this.context = con;
        this.feed = assets;
    }

    //Sets the the individual feed entries to the items in the view
    public View getView(int position, View convertView, ViewGroup group)
    {
        //Gets feed item
        FeedData item = feed.get(position);

        //Initialises the list item view
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_list_item, null);

        //Set title text & description view entries
        TextView title = (TextView) view.findViewById(R.id.txt_Title);
        TextView description  = (TextView)view.findViewById(R.id.itm_details);



        //Trims the length of the description to provide brief details
        int descriptionLength = item.getDescription().length();

        if(descriptionLength > 100){
            String detail = item.getDescription().substring(0, 100)+ "...";
            description.setText(detail);
        }
        else description.setText(item.getDescription());

        //When the item exceeds the 7 day limit
        if(item.getDuration() >=7){
            view.setBackgroundColor(Color.parseColor("#DC6161"));
        }
        else
        if(item.getDuration() <= 3) view.setBackgroundColor(Color.parseColor("#8BD28E"));
        else if (item.getDuration()> 3 && item.getDuration() < 7)
            view.setBackgroundColor(Color.parseColor("#FFB266"));
        

        title.setText(item.getTitle());



        return view;
    }


}
